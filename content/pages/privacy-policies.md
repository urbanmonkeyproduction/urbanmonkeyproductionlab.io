+++
title = 'Privacy Policies'
date = 2024-03-17T16:34:58-07:00
draft = false
type = "page"
+++

#### Tippers

Read the [privacy policy](/pages/privacy-policy-tippers) for the Tipper apps.

#### Dad Jokes Amazing

Read the [privacy policy](/pages/privacy-policy-dad-jokes-amazing) for the Dad Jokes Amazing apps.

#### Esperanto Words

Read the [privacy policy](/pages/privacy-policy-esperanto-words) for the Esperanto Words apps.

#### Bible Verses Amazing

Read the [privacy policy](/pages/privacy-policy-bible-verses-amazing) for the Bible Verses Amazing apps.

#### Tab Manager Amazing

Read the [privacy policy](/pages/privacy-policy-tab-manager-amazing) for the Tab Manager Amazing apps.