+++
title = 'About Page'
date = 2023-09-01T16:14:54-07:00
draft = false
type = "page"
+++

## Goals

Our primary objective is to create a diverse portfolio of applications. Each project aims to solve unique problems or bring a bit of delight to the user. With a focus on quality and user experience, the goal is to create apps that could potentially gain significant traction and popularity.

---

For collaboration inquiries or additional information, feel free to [contact us](/pages/contact).

