+++
title = 'Privacy Policy for Tab Manager Amazing'
date = 2023-09-01T17:20:01-07:00
draft = false
type = "page"
+++

## Content

**Last Updated: September 1st, 2023**

### Introduction

Welcome to the Tab Manager Amazing apps! We know how much you value your privacy, so we're committed to protecting it. Although we don't collect any personal information from you, we do use third-party services that may collect data. This Privacy Policy explains how we comply with privacy laws, including international privacy regulations such as GDPR and CCPA.

### Data We Collect

As of now, we don't collect any personal data from our users within the Tab Manager Amazing app. All content is pre-bundled in the app, and no external APIs are used for this purpose.

### Third-Party Services

We use Google AdMob to display banner ads in the Tab Manager Amazing App. Google AdMob may collect data as per their own Privacy Policy. For more information on how Google uses information from sites or apps that use their services, you can visit Google's Privacy & Terms webpage.

### Data Storage and Security

Although we don't collect data, any data that may be stored in the future will be stored within the app using HiveDB.

### User Rights

Since we don't collect any personal data, user rights such as data modification or deletion are not applicable at this time.

### Consent

Currently, we don't collect any personal information, so user consent for data collection is not required.

### International Users

This app is accessible to users outside of the United States. We comply with international privacy laws, including but not limited to GDPR and CCPA.

### Contact Us

If you have any questions about this Privacy Policy, please contact us using our [contact form](/pages/contact).
