+++
title = 'Privacy Policy for Tippers'
date = 2024-03-17T16:34:58-07:00
draft = false
type = "page"
+++

## Content

**Last Updated: March 17th, 2024**

### Introduction

Welcome to the Tippers apps! We know how much you value your privacy, so we're committed to protecting it. Although we don't collect any personal information from you, we do use third-party services that may collect data. This Privacy Policy explains how we comply with privacy laws, including international privacy regulations such as GDPR and CCPA.

### Data We Collect

As of now, we don't collect any personal data from our users within the Tippers app.

### Third-Party Services

We use Google AdMob to display banner ads in the Tippers App. Google AdMob may collect data as per their own Privacy Policy. For more information on how Google uses information from sites or apps that use their services, you can visit Google's Privacy & Terms webpage.

### Data Storage and Security

Although we don't collect data, any data that may be stored in the future will be stored within the app using HiveDB.

### User Rights

Since we don't collect any personal data, user rights such as data modification or deletion are not applicable at this time.

### Consent

Currently, we don't collect any personal information, so user consent for data collection is not required.

### International Users

This app is accessible to users outside of the United States. We comply with international privacy laws, including but not limited to GDPR and CCPA.

### Contact Us

If you have any questions about this Privacy Policy, please contact us using our [contact form](/pages/contact).
